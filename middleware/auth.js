const jwt = require('jsonwebtoken')
const User = require('../models/User')
const utils = require('../_helpers/utils')

const auth = async (req, res, next) => {
    //Verify token from header
    try {
        const token = utils.getCookie("token", req.headers.cookie);
        const data = jwt.verify(token, utils.JWT_key);
        const user = await User.findOne({ _id: data._id, 'tokens.token': token });
        if (!user) {
            throw new Error();
        }
        req.user = user;
        req.token = token;
        next();
    } catch (error) {
        res.status(401).send({ error: 'Not authorized to access this resource' });
    }

}

module.exports = auth