var config = {};

config.port = 8080;
config.dbstring = "mongodb://admin:Admin123@ds233288.mlab.com:33288/chat_node";
config.JWT_key = "A very secure password";
config.botURI = "http://localhost:8081/";
config.botName = "Bot";
config.getCookie = (cname, cookies) => {
  //Cookie selector
  var name = cname + "=";
  var ca = cookies.split(';');
  for (var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}

module.exports = config;
