const mongoose = require('mongoose')
const utils = require('../_helpers/utils')

mongoose.connect(utils.dbstring, {
    useNewUrlParser: true,
    useCreateIndex: true,
})