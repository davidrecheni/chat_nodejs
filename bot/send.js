const express = require('express');
const app = express();
const axios = require('axios');

const http = require('http').Server(app);

const utils = require('./utils');
//Settings
app.use(express.json());

app.get('/:stock_code', function (req, res) {
  //Gets param and sends GET to service
  const code = req.params.stock_code;
  axios.get('https://stooq.com/q/l/?s=' + code + '&f=sd2t2ohlcv&h&e=csv')
    .then(response => {
      if (response.data != null) {
        let data = utils.CSVtoArray(response.data);
        res.send(data);
      }
    })
    .catch(error => {
      console.log(error);
      res.send(error);
    });

});

const server = http.listen(utils.port, () => {
  console.log(`Server running on port ${utils.port}`);
});
