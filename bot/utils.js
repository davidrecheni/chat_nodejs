let config = {};

config.port = 8081;
config.username = "bot";
config.password = "bot";
config.loginURL = "http://localhost:8080/users/login";

config.CSVtoArray = (file) => {
  //Formats CSV string to Array
  if (!file) return "invalid CSV";
  
  let response = [];
  let f = file.split("\n");
  f.pop(); // remove eof
  f.forEach((stock, index) => {
    stock = stock.replace("\r", "")
    stock = stock.split(",")
    response.push(stock)
  })
  return response;
}



module.exports = config;
