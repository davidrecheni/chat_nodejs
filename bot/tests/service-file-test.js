const chai = require('chai');
const expect = chai.expect; // we are using the "expect" style of Chai
const Stock_code = require('../utils');

describe('Stock_code', function() {
  it('CSVtoArray() should return "invalid CSV" if no items are passed in', function() {
    expect(Stock_code.CSVtoArray()).to.equal("invalid CSV");
    expect(Stock_code.CSVtoArray("")).to.equal("invalid CSV");
    expect(Stock_code.CSVtoArray(undefined)).to.equal("invalid CSV");
    expect(Stock_code.CSVtoArray(null)).to.equal("invalid CSV");
  });
  it('CSVtoArray() should return "invalid CSV" when passing !string', function() {
    expect(Stock_code.CSVtoArray(0)).to.equal("invalid CSV");
  });
  it('CSVtoArray() should return an array when passing a CSV', function() {
    let csvString = "Symbol,Date,Time,Open,High,Low,Close,Volume\r\nAAPL.US,2019-10-14,17:56:29,234.9,238.1342,234.6701,237.16,10259206\r\n";
    expect(Stock_code.CSVtoArray(csvString)).to.be.a("array");
  });

});