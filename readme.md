# Chatroom with simple login and chatbot connected to Stock market API
- NodeJS ,Socket I.O, Serverside rendering, MongoDB, JQuery
- Unit tests mocha+chai
# Use

- Users

- - POST /users to register a new user.
 
  request body example:
  
        -  {
        -  "username": "ExampleUser",
        -  "password": "ExamplePassword"
        -  }
      
- -   already registered users:
    
        - tester:tester
        
        - tester2:tester2
        
        - bot:bot
    

# Bot use

Install dependencies and run( /bot folder)

Sending a GET request to the following URL you can add Stock quotations from an API to the chat
 - /chat/stock=STOCK_CODE for running commands

Example: http://127.0.0.1:8080/chat/stock=WIG

# Unit Test

Run tests from /bot folder:

- mocha tests --recursive 