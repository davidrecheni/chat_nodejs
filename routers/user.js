const express = require('express')
const User = require('../models/User')
const usersController = require('../controllers/usersController')
const router = express.Router()

router.post('/users', (req, res) => {
   //Register User
   usersController.newUser(req, res)
})

router.post('/users/login', async (req, res) => {
   //Login a registered user
   usersController.login(req, res)
})

module.exports = router
