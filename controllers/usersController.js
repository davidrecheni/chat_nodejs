let controller = {};
const User = require('../models/User')

controller.newUser = async (req, res) => {
    try {
        //Generate user
        const user = new User(req.body)
        //Store
        await user.save()
        //Generate token
        const token = await user.generateAuthToken()
        res.status(201).send({ user, token })
    } catch (error) {
        res.status(400).send(error)
    }
}

controller.login = async (req, res) => {
    try {
        //Get credentials from request
        let username = req.body.filter(item => item.name == "user")[0].value
        let pass = req.body.filter(item => item.name == "pass")[0].value

        //Check credentials
        const user = await User.findByCredentials(username, pass);

        //Not found error
        if (!user) {
            return res.status(401).send({ error: 'Login failed! Check authentication credentials' })
        }

        //On success assign token to cookies
        const token = await user.generateAuthToken()
        res.cookie('token', token, { httpOnly: true })
            .sendStatus(200);

    } catch (error) {
        res.status(400).send(error)
    }
}
module.exports = controller;
