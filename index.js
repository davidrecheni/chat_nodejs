const express = require('express');
const app = express();
const axios = require('axios');

const http = require('http').Server(app);
const io = require('socket.io')(http);

const userRouter = require('./routers/user');
const utils = require('./_helpers/utils');
const auth = require('./middleware/auth');

//Settings
require('./_helpers/database');
app.use(express.json());
app.use(userRouter);


app.get('/', function (req, res) {
    res.render('login.ejs');
});

app.get('/chat', auth, function (req, res) {
    app.locals.username = req.user.username;
    res.render('index.ejs');
});

app.get('/chat/stock=:stock_code', auth, function (req, res) {
    let timestamp = new Date();
    const code = req.params.stock_code;
    axios.get(utils.botURI + code)
        .then(response => {
            if (response.data != undefined) {
                response.data.forEach((row, index) => {
                    if (index == 0) {
                        //Headers
                    } else {
                        //Rows
                        if (row[1] != "N/D") {
                            io.emit('chat_message', [timestamp.getTime(), '<i>' + timestamp + '</i> <strong>' + utils.botName + '</strong>: ' + row[0] + ' quote is $' + row[6] + ' per share']);
                        } else {
                            io.emit('chat_message', [timestamp.getTime(), '<i>' + timestamp + '</i> <strong>' + utils.botName + '</strong>: ' + row[0] + ' quote not found']);
                        }
                    }
                })
            } else {
                console.log("Error: ", response);

            }
        })
        .catch(error => {
            console.log(error);

        });

});

io.sockets.on('connection', function (socket) {

    socket.username = app.locals.username;
    var timestamp = new Date();
    var currentRoom = 'one';
    socket.join('one');
    io.to(currentRoom).emit('chat_message', [timestamp.getTime(), '<i>' + timestamp + '</i>' + '  <strong>' + socket.username + ' Joined the room ' + currentRoom + '</strong>']);

    socket.on('join_room', () => {
        if (currentRoom == 'two') {
            socket.join('one')
            socket.leave('two')
            currentRoom = 'one'
        } else {
            socket.join('two')
            socket.leave('one')
            currentRoom = 'two'
        }
        io.to(currentRoom).emit('chat_message', [timestamp.getTime(), '<i>' + timestamp + '</i>' + '  <strong>' + socket.username + ' Joined the room ' + currentRoom + '</strong>']);
    })

    socket.on('chat_message', function (message) {
        timestamp = new Date();
        io.to(currentRoom).emit('chat_message', [timestamp.getTime(), '<i>' + timestamp + '</i>' + '  <strong>' + socket.username + '</strong>: ' + message]);
    });

});

const server = http.listen(utils.port, () => {
    console.log(`Server running on port ${utils.port}`);
});
